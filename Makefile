.DEFAULT_GOAL := help
.PHONY : resources

OWNER=notes
SERVICE_NAME=python

## RESULT_VARS ##
DOCKER_NETWORK 	= base_network
PROJECT_NAME	= $(OWNER)-$(SERVICE_NAME)
export CONTAINER_NAME 	= $(PROJECT_NAME)_backend
export IMAGE_DEV		= $(PROJECT_NAME):dev
export ENV 		   ?= dev


## Target Commons ##
build: ## Build image for evaluationbackend project: make build
	cp app/requeriments.txt docker/dev/resources/requeriments.txt
	docker build -f docker/dev/Dockerfile -t $(IMAGE_DEV) docker/dev/
	rm -f docker/dev/resources/requirements.txt

ssh: ## Connect ssh
	docker exec -it $(CONTAINER_NAME) bash

up: ## Run container in background
	@make verify-network &> /dev/null
	DOCKER_NETWORK=$(DOCKER_NETWORK) \
	docker-compose -p $(SERVICE_NAME) up -d backend

down: ## Remove container
	@IMAGE_DEV=$(IMAGE_DEV) \
	CONTAINER_NAME=$(CONTAINER_NAME) \
	DOCKER_NETWORK=$(DOCKER_NETWORK) \
	docker-compose -p $(SERVICE_NAME) down

restart: ## Restart all containers, use me with: make restart
	@IMAGE_DEV=$(IMAGE_DEV) \
	CONTAINER_NAME=$(CONTAINER_NAME) \
	DOCKER_NETWORK=$(DOCKER_NETWORK) \
	docker-compose -p $(SERVICE_NAME) restart

verify-network: ## Verify the local network was created in docker: make verify_network
	@if [ -z $$(docker network ls | grep $(DOCKER_NETWORK) | awk '{print $$2}') ]; then\
		(docker network create $(DOCKER_NETWORK));\
	fi

## Migrate ##
revision: ## Create a new revision
	@docker run --rm  -t -u $(UID_LOCAL):$(GID_LOCAL) \
			-v $(PWD)/app:/app:rw \
			--entrypoint /resources/alembic.sh $(IMAGE_DEV) revision -m "$(DESC)"

migrate: ## Execute migrate
	@docker run --rm -t -u $(UID_LOCAL):$(GID_LOCAL) \
			-v $(PWD)/app:/app:rw \
			--network="host" \
			--entrypoint /resources/alembic.sh $(IMAGE_DEV) upgrade head

migrate-id: ## Execute migrate
	@docker run --rm  -t -u $(UID_LOCAL):$(GID_LOCAL) \
			-v $(PWD)/app:/app:rw --network="host" \
			--entrypoint /resources/alembic.sh $(IMAGE_DEV) upgrade $(ID)

rollback: ## Execute migrate rollback
	@docker run --rm -t -u $(UID_LOCAL):$(UID_LOCAL) -t \
			-v $(PWD)/app:/app:rw --network="host" \
			--entrypoint /resources/alembic.sh $(IMAGE_DEV) downgrade -1

## Target Help ##
help:
	@printf "\033[31m%-22s %-59s %s\033[0m\n" "Target" " Help" "Usage"; \
	printf "\033[31m%-22s %-59s %s\033[0m\n"  "------" " ----" "-----"; \
	grep -hE '^\S+:.*## .*$$' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' | sort | awk 'BEGIN {FS = ":"}; {printf "\033[32m%-22s\033[0m %-58s \033[34m%s\033[0m\n", $$1, $$2, $$3}'
