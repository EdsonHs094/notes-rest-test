# -*- coding: utf-8 -*-
import dependency_injector.containers as containers
import dependency_injector.providers as providers

from notes.application.services.notes import SaveNotesService, GetAllNotesService, GetAllNotesByUserService
from notes.application.services.users import CreateUserService, LoginUserService

from notes.domain.services.notes import NotesDomainService
from notes.domain.services.users import UsersDomainService

from notes.infraestructure.peewee.notes import Notes
from notes.infraestructure.peewee.users import Users


class RepositoryInjector(containers.DeclarativeContainer):
    notes = providers.Singleton(Notes)
    users = providers.Singleton(Users)


class DomainServicesInjector(containers.DeclarativeContainer):
    notes = providers.Singleton(NotesDomainService,
                                repository=RepositoryInjector.notes)
    users = providers.Singleton(UsersDomainService,
                                repository=RepositoryInjector.users)


class AppServicesInjector(containers.DeclarativeContainer):
    get_notes = providers.Singleton(GetAllNotesService,
                                    domain_service=DomainServicesInjector.notes)
    get_notes_by_user = providers.Singleton(GetAllNotesByUserService,
                                    domain_service=DomainServicesInjector.notes)
    login_user = providers.Singleton(LoginUserService,
                                    domain_service=DomainServicesInjector.users)

    create_notes = providers.Singleton(SaveNotesService,
                                       domain_service=DomainServicesInjector.notes)
    create_user = providers.Singleton(CreateUserService,
                                           domain_service=DomainServicesInjector.users)
