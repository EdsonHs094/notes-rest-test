from _md5 import md5
import jwt

secret = 'secrete_jwt'


def encrypt_password(password):
    return md5(password.encode('utf-8')).hexdigest()


def jwt_encoded(dict_enc):
    return jwt.encode(dict_enc, secret, algorithm='HS256')


def jwt_decode(jwt_dec):
    try:
        return jwt.decode(jwt_dec, secret, algorithms=['HS256'])
    except Exception:
        return ''