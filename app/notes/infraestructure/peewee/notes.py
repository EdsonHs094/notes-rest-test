from peewee import CharField, IntegerField
from notes.infraestructure.adapter.peewee import BaseModel


class Notes(BaseModel):
    message = CharField()
    name = CharField()
    author = IntegerField()

    def get_all(self):
        return Notes.select().execute()

    def get_notes_by_user(self, id_user):
        return Notes.select().where(Notes.author == id_user).execute()

    def create(self, note):
        return Notes.insert(message=note['message'], name=note['name'], author=note['author']).execute()
