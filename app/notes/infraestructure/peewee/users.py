from peewee import CharField
from notes.infraestructure.adapter.peewee import BaseModel
from notes.infraestructure.utils import encrypt_password


class Users(BaseModel):
    username = CharField(unique=True)
    password = CharField()

    def login_user(self, username, password):
        try:
            pw_hash = encrypt_password(password)
            user = Users.get(
                (Users.username == username) &
                (Users.password == pw_hash))
            return {
                'username': user.username,
                'id': user.id
            }
        except Users.DoesNotExist as err:
            raise err

    def create(self, user):
        return Users.insert(username=user['username'], password=encrypt_password(user['password'])).execute()
