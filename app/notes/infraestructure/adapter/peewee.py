import datetime
import os
from peewee import *
from hashlib import md5


path = os.path.dirname(os.path.abspath(__file__))
db = os.path.join(path, 'notes-test.db')
sqlite_db = SqliteDatabase(db, pragmas={'journal_mode': 'wal'})


class BaseModel(Model):
    """A base model that will use our Sqlite database."""

    class Meta:
        database = sqlite_db
