from .base import BaseConfig
from bootstrap import YamlFile
import collections
import glob


def dict_merge(dct, merge_dct):
    for k, v in merge_dct.items():
        if (k in dct and isinstance(dct[k], dict)
                and isinstance(merge_dct[k], collections.Mapping)):
            dict_merge(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]


class ConfigYaml(BaseConfig):
    configs = {}
    yaml_file = YamlFile()
    paths = glob.glob('config/*yml')
    for path in paths:
        config = yaml_file.read(path)
        dict_merge(configs, config)

    def __init__(self):
        self._config = self.configs
