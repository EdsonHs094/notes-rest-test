class NotesDomainService(object):

    def __init__(self, repository):
        self.__repository = repository

    def get_all_notes(self):
        return self.__repository.get_all()

    def get_notes_by_user(self, id_user):
        return self.__repository.get_notes_by_user(id_user)

    def create(self, new_note):
        result = self.__repository.create(new_note)
        new_note['id'] = result
        return new_note
