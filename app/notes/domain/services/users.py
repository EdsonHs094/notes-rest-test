from notes.infraestructure.utils import jwt_encoded

class UsersDomainService(object):

    def __init__(self, repository):
        self.__repository = repository

    def login_user(self, user, password):
        user = self.__repository.login_user(user, password)
        jwt = jwt_encoded(user)
        return jwt

    def create(self, new_user):
        result = self.__repository.create(new_user)
        new_user['id'] = result
        return new_user
