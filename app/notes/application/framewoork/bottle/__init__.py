import bottle, json
from bottle import route, response, get, post, request
from notes.application.singleton.jwt_verification import JwtVerification
from notes.infraestructure.dependecy_injection.container import AppServicesInjector
from notes.infraestructure.utils import jwt_decode

get_notes_service = AppServicesInjector.get_notes()
get_notes_by_user = AppServicesInjector.get_notes_by_user()
create_notes = AppServicesInjector.create_notes()
login_user = AppServicesInjector.login_user()
create_user = AppServicesInjector.create_user()


@get('/notes')
def get_notes_handler():
    service_response = get_notes_service.execute()
    notes = []
    for note in service_response:
        notes.append(
            {
                'id': note.id,
                'message': note.message,
                'author': note.author,
                'name': note.name,
            }
        )

    """Handles GET requests"""
    quote = {
        'message': (
            "Peticion Exitosa"
        ),
        'result': notes
    }

    response.headers['Content-Type'] = 'application/json'
    response.headers['Cache-Control'] = 'no-cache'
    return json.dumps(quote)


@get('/users/<id_user>/notes')
def get_notes_handler(id_user):
    auth_token = request.get_header('Authorization')
    payload = jwt_decode(auth_token)
    if not payload:
        return json.dumps({
            'message': 'taken incorrect'
        })

    if payload['id'] != int(id_user):
        return json.dumps({
            'message': 'request Unauthorized'
        })
    service_response = get_notes_by_user.execute(id_user)
    notes = []
    for note in service_response:
        notes.append(
            {
                'id': note.id,
                'message': note.message,
                'author': note.author,
                'name': note.name,
            }
        )

    """Handles GET requests"""
    quote = {
        'message': (
            "Peticion Exitosa"
        ),
        'result': notes
    }

    response.headers['Content-Type'] = 'application/json'
    response.headers['Cache-Control'] = 'no-cache'
    return json.dumps(quote)


@post('/notes')
def create_notes_handler():
    try:
        data = request.json
        response_note = create_notes.execute(data)
        note = {
            'id': response_note['id'],
            'message': response_note['message'],
            'author': response_note['author'],
            'name': response_note['name']
        }

        quote = {
            'message': (
                "Peticion Exitosa"
            ),
            'result': note
        }
    except Exception as e:
        quote = {
            'message': (
                "Peticion denegada"
            ),
            'result': e.messages
        }
    response.headers['Content-Type'] = 'application/json'
    response.headers['Cache-Control'] = 'no-cache'
    return json.dumps(quote)


@post('/users')
def create_users_handler():
    data = request.json
    response_user = create_user.execute(data)
    user = {
        'id': response_user['id'],
        'username': response_user['username']
    }

    quote = {
        'message': (
            "Peticion Exitosa"
        ),
        'result': user
    }
    response.headers['Content-Type'] = 'application/json'
    response.headers['Cache-Control'] = 'no-cache'
    return json.dumps(quote)


@post('/login')
def login_handler():
    try:
        data = request.json
        response_user = login_user.execute(data['username'], data['password'])
        user = {
            'access_token': response_user.decode('utf-8'),
            "token_type": "bearer"
        }
        quote = {
            'message': (
                "Peticion Exitosa"
            ),
            'result': user
        }
    except Exception as e:
        quote = {
            'message': (
                "Unauthorized"
            ),
        }
    response.headers['Content-Type'] = 'application/json'
    response.headers['Cache-Control'] = 'no-cache'
    return json.dumps(quote)



class BottleApi:
    def __init__(self):
        self.api = bottle.default_app()
