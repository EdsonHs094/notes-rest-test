class LoginUserService(object):

    def __init__(self, domain_service):
        self.__domain_service = domain_service

    def execute(self, user, password):
        return self.__domain_service.login_user(user, password)


class CreateUserService(object):

    def __init__(self, domain_service):
        self.__domain_service = domain_service

    def execute(self, user):
        return self.__domain_service.create(user)
