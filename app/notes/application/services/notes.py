from marshmallow import Schema, fields, ValidationError


class NotesSchema(Schema):
    message = fields.Str()
    name = fields.Str()
    author = fields.Integer()


class GetAllNotesService(object):

    def __init__(self, domain_service):
        self.__domain_service = domain_service

    def execute(self):
        return self.__domain_service.get_all_notes()


class GetAllNotesByUserService(object):

    def __init__(self, domain_service):
        self.__domain_service = domain_service

    def execute(self, id_user):
        return self.__domain_service.get_notes_by_user(id_user)


class SaveNotesService(object):

    def __init__(self, domain_service):
        self.__domain_service = domain_service

    def execute(self, note):
        try:
            schema = NotesSchema().load(note)
            return self.__domain_service.create(schema)
        except ValidationError as err:
            raise err
