# -*- coding: utf-8 -*-
""" Init Bootstrap """
import json
import yaml


class File(object):
    """ File """
    def read(self, file):
        """ read """
        return open(file)


class YamlFile(File):
    """ YamlFile """
    def read(self, file):
        """ read """
        file_object = super(YamlFile, self).read(file)
        return yaml.load(file_object)


class JsonFile(File):
    """ JsonFile """
    def read(self, file):
        """ read """
        file_object = super(JsonFile, self).read(file)
        return json.load(file_object)
