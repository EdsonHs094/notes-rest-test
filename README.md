### Instalar Aplicacion

Generar la imagen ejecutamos el comando build
```sh
$ make build
```

Levantamos el aplicativo, Se levantara en localhost:9000
```sh
$ make up
```

Para verificar entrar a localhost:9001

### Uso

Este servicio cuenta con los siguientes request

  - /: ruta base donde da un mensaje simple
  - /notes: metodos GET y POST para la creacion y obtencion de notas
  - /login: usar username y password brinda un token de acceso
  - /users: POST para crear un usuario
  
  El listado de notas se le debe pasar un token de la siguiente manera en la cabecera
  Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImVkc29uaHMiLCJpZCI6MX0.v3mXIdrBcWgdjk350uVrn21DJMbIwa9YR_yrpkBx8kg'
   
  No se a usado migraciones por efectos practicos, el repositorio esta dockerizado se a intentado usar una arquitectura ddd,
  adaptando el comportamiento del framework y del orm, personalmente yo hubiera elegido sqlalchemy como orm y flask como framework
  estan todos los requerimientdos desde validacion hasta verificacion, el unico faltante es la capa fronted por falta de tiempo, en caso de fronted me hubiera gustado usar react para la capa frontend
  Saludos